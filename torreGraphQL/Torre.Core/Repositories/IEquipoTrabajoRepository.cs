﻿using Common.Utils.Repository;
using Torre.Core.Models;

namespace Torre.Persistence.Repositories
{
    public interface IEquipoTrabajoRepository : IRepository<EquipoTrabajo>
    {
    }
}
