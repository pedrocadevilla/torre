﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Torre.Core.Models
{
    [Table("EQUIPO_TRABAJO")]
    public partial class EquipoTrabajo
    {
        [Column("ID")]
        public long Id { get; set; }
        [StringLength(50)]
        public string PersonId { get; set; }
        [StringLength(50)]
        public string JobId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaCreacion { get; set; }
    }
}
