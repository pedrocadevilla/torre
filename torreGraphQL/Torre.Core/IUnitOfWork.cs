﻿using Torre.Persistence.Repositories;
using System;
using System.Threading.Tasks;

namespace Torre.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IEquipoTrabajoRepository EquipoTrabajo { get; }

        Task<int> Complete();
    }
}
