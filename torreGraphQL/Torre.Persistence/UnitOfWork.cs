﻿using Torre.Core;
using Torre.Persistence.Repositories;
using System.Threading.Tasks;

namespace Torre.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TORREContext _Context;

        public IEquipoTrabajoRepository EquipoTrabajo { get; private set; }
      
        public UnitOfWork(TORREContext Context)
        {
            EquipoTrabajo = new EquipoTrabajoRepository(_Context);
        }

        public async Task<int> Complete()
        {
            return await _Context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _Context.Dispose();
        }
    }
}
