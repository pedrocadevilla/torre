﻿using Microsoft.EntityFrameworkCore;
using Torre.Core.Models;

namespace Torre.Persistence
{
    public partial class TORREContext : DbContext
    {
        public TORREContext()
        {
        }

        public TORREContext(DbContextOptions<TORREContext> options)
            : base(options)
        {
        }

        public virtual DbSet<EquipoTrabajo> EquipoTrabajo { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=DESKTOP-HNB9D0P\\SQLEXPRESS;Database=TORRE;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");
        }
    }
}