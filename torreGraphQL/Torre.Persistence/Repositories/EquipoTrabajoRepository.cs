﻿using Common.Utils.Repository;
using Microsoft.EntityFrameworkCore;
using Torre.Core.Models;

namespace Torre.Persistence.Repositories
{
    class EquipoTrabajoRepository : Repository<EquipoTrabajo>, IEquipoTrabajoRepository
    {

        public EquipoTrabajoRepository(DbContext context) : base(context)
        { }
    }
}
