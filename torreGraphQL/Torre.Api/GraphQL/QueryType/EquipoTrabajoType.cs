﻿using HotChocolate.Types;
using Torre.Core.Models;

namespace Torre.Api.GraphQL.QueryType
{
    public class EquipoTrabajoType : ObjectType<EquipoTrabajo>
    {
        protected override void Configure(IObjectTypeDescriptor<EquipoTrabajo> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("ActividadLaborales de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.PersonId);
            descriptor.Field(x => x.JobId);
            descriptor.Field(x => x.FechaCreacion);
        }
    }
}
