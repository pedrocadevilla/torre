﻿using HotChocolate.Types;
using Torre.Core.Models;

namespace Torre.Api.GraphQL.QueryType
{
    public class QueryType : ObjectType<Query>
    {
        protected override void Configure(IObjectTypeDescriptor<Query> descriptor)
        {
            descriptor.Name("Consultas");
            descriptor.Description("Conjunto de elementos acerca de los cuales se puede obtener informaciòn");
        }
    }
}
