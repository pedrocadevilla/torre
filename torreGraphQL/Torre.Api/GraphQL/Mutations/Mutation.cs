﻿using System.Threading.Tasks;
using Torre.Core;
using Torre.Core.Models;

namespace Torre.Api.GraphQL.Mutations
{
    public partial class Mutation
    {
        private readonly IUnitOfWork _unitOfWork;

        public Mutation(IUnitOfWork unitOfWork)
        { 
            _unitOfWork = unitOfWork;
        }

        public async Task<EquipoTrabajo> CrearEquipoTrabajoAsync(EquipoTrabajo equipo)
        {
            _unitOfWork.EquipoTrabajo.Add(equipo);
            await _unitOfWork.Complete();
            return equipo;
        }
    }
}
