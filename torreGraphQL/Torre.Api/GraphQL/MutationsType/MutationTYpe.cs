﻿using HotChocolate.Types;
using Torre.Api.GraphQL.InputTypes;
using Torre.Api.GraphQL.Mutations;
using Torre.Api.GraphQL.QueryType;

namespace Torre.Api.GraphQL.MutationsType
{
    public class MutationType : ObjectType<Mutation>
    {
        protected override void Configure(IObjectTypeDescriptor<Mutation> descriptor)
        {
            descriptor.Name("Mutaciones");
            descriptor.Description("Operaciones para guardar, modificar y Remove registros");

            descriptor.Field(t => t.CrearEquipoTrabajoAsync(default))
                .Type<NonNullType<EquipoTrabajoType>>()
                .Argument("equipo", a => a.Type<NonNullType<EquipoTrabajoInputType>>())
                .Description("Permite crear un nuevo equipo de trabajo. Recibe los id de persona y trabajo");
        }
    }
}
