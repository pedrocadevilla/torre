﻿using HotChocolate.Types;
using Torre.Core.Models;

namespace Torre.Api.GraphQL.InputTypes
{
    public class EquipoTrabajoInputType : InputObjectType<EquipoTrabajo>
    {
        protected override void Configure(IInputObjectTypeDescriptor<EquipoTrabajo> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("EquipoTrabajoInput");

            descriptor.Field(x => x.PersonId);
            descriptor.Field(x => x.JobId);
        }
    }
}
