import axios from 'axios'
const { createAsyncThunk } = require("@reduxjs/toolkit");

export const createTeam = createAsyncThunk(
    "team/createTeam",
    (body) => {
        axios({
            url: 'https://localhost:5003/',
            method: 'post',
            data: {
                query:`
                mutation crearEquipoTrabajo {
                    crearEquipoTrabajo(equipo: { personId: "` + body.personId + `", jobId:"` + body.jobId + `"}) {
                        id
                    }
                }`
            }
        })
        .then(json => json)
    }
)
