const { createAsyncThunk } = require("@reduxjs/toolkit");

export const getAllPersons = createAsyncThunk(
    "person/getAllPersons",
    (offset) => {
        return fetch('https://search.torre.co/people/_search/?offset=' + (offset-1)*4 + '&size=4&aggregate=0', {method:'POST', headers: {"Content-Type": "application/json"}})
        .then(response => {
            if(!response.ok) throw Error(response.statusText);
            return response.json();
        })
        .then(json => json);
    }
)

export const getPersons = createAsyncThunk(
    "person/getPersons",
    (id) => {
        return fetch('https://torre.bio/api/bios/' + id)
        .then(response => {
            if(!response.ok) throw Error(response.statusText);
            return response.json();
        })
        .then(json => json);
    }
)