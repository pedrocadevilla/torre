const { createAsyncThunk } = require("@reduxjs/toolkit");

export const getAllOpportunities = createAsyncThunk(
    "job/getAllOpportunities",
    (offset) => {
        return fetch('https://search.torre.co/opportunities/_search/?offset=' + (offset-1)*4 + '&size=4&aggregate=0', {method:'POST', headers: {"Content-Type": "application/json"}})
        .then(response => {
            if(!response.ok) throw Error(response.statusText);
            return response.json();
        })
        .then(json => json);
    }
)

export const getOpportunities = createAsyncThunk(
    "job/getOpportunities",
    (id) => {
        return fetch('https://torre.co/api/opportunities/' + id)
        .then(response => {
            if(!response.ok) throw Error(response.statusText);
            return response.json();
        })
        .then(json => json);
    }
)