import { createSlice } from '@reduxjs/toolkit'
import { getAllOpportunities, getOpportunities } from '../actions/jobActions'
import { getAllPersons, getPersons } from '../actions/personActions'
import { createTeam } from '../actions/equipoTrabajoActions'
import history from '../history'

const teamSlice = createSlice({
    name: "teamReducer",
    initialState: {
        jobs: [],
        selectedJob: {},
        selectedPerson: {},
        persons: [],
        jobDetail: {},
        personDetail: {},
        loading: true,
        jobDialog: false,
        personDialog: false,
        opportunityPage: 1,
        personPage: 1,
        countJobs: 0,
        countPersons: 0

    },
    reducers: {
        goToThanks: (state) => {
            history.push('/thanks');
        },
        setInitialState: (state) => {
            state.selectedJob = {};
            state.personDetail = {};
            state.personPage = 1;
            state.opportunityPage = 1;
            state.jobDialog = false;
            state.personDialog = false;
            state.jobs = [];
            state.persons = [];
            history.push('/');
        },
        closeJobDialog: (state) => {
            state.jobDialog = false;
        },
        closePersonDialog: (state) => {
            state.personDialog = false;
        },
        changeOpportunityPage: (state, val) => {
            state.opportunityPage = val.payload;
        },
        changePersonPage: (state, val) => {
            state.personPage = val.payload;
        },
        selectJob: (state) => {
            state.selectedJob = state.jobDetail;
            state.jobDialog = false;
        }
    },
    extraReducers: {
        [getAllOpportunities.pending]: state => {
            state.loading = true
        },
        [getAllOpportunities.rejected]: (state, action) => {
            state.loading = false;
            state.error = action.error;
        },
        [getAllOpportunities.fulfilled]: (state, action) => {
            state.loading = false;
            state.jobs = action.payload.results;
            state.countJobs = action.payload.total;
        },
        [getAllPersons.pending]: state => {
            state.loading = true
        },
        [getAllPersons.rejected]: (state, action) => {
            state.loading = false;
            state.error = action.error;
        },
        [getAllPersons.fulfilled]: (state, action) => {
            state.loading = false;
            state.persons = action.payload.results;
            state.countPersons = action.payload.total;
        },
        [getOpportunities.pending]: state => {
            state.loading = true
        },
        [getOpportunities.rejected]: (state, action) => {
            state.loading = false;
            state.error = action.error;
        },
        [getOpportunities.fulfilled]: (state, action) => {
            state.loading = false;
            state.jobDialog = true;
            state.jobDetail = action.payload;
        },
        [getPersons.pending]: state => {
            state.loading = true
        },
        [getPersons.rejected]: (state, action) => {
            state.loading = false;
            state.error = action.error;
        },
        [getPersons.fulfilled]: (state, action) => {
            state.loading = false;
            state.personDialog = true;
            state.personDetail = action.payload;
        },
        [createTeam.pending]: state => {
            state.loading = true;
        },
        [createTeam.rejected]: (state, action) => {
            state.loading = false;
            state.error = action.error;
        },
        [createTeam.fulfilled]: (state, action) => {
            state.loading = false;
            state.personDialog = false;
        }
    }
})

export const { closeJobDialog, selectJob, changePersonPage, changeOpportunityPage, closePersonDialog, setInitialState, goToThanks } = teamSlice.actions
export default teamSlice.reducer