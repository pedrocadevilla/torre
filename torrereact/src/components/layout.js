import React from 'react'
import Container from '@material-ui/core/Container'
import { makeStyles } from '@material-ui/core/styles'

const properStyles = makeStyles((theme) => ({
    content: {
        maxWidth: '90%',
        marginTop: theme.spacing(3),
        marginLeft: '5%'
    },
}))

export const Layout = (props) => {
	const classes = properStyles()
	return (
		<div>
			<Container className={classes.content}>
				{ props.children }
			</Container>
		</div>
	)
}

export default Layout